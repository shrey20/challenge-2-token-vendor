pragma solidity 0.8.4;
// SPDX-License-Identifier: MIT

import "@openzeppelin/contracts/access/Ownable.sol";
import "./YourToken.sol";

contract Vendor is Ownable {
    event BuyTokens(address buyer, uint256 amountOfETH, uint256 amountOfTokens);
    event SellTokens(address user, uint256 amountOfEth, uint256 amountOfTokens);
    YourToken public yourToken;
    uint256 public constant tokensPerEth = 100;

    constructor(address tokenAddress) {
        yourToken = YourToken(tokenAddress);
    }

    // ToDo: create a payable buyTokens() function:
    function buyTokens() public payable {
        uint256 amountOfEth = msg.value;
        uint256 amountOfTokens = amountOfEth * tokensPerEth;
        uint256 vendorBalance = yourToken.balanceOf(address(this));
        require(
            vendorBalance >= amountOfTokens,
            "Vendor do not have enough balance"
        );
        address buyer = msg.sender;
        bool sent = yourToken.transfer(buyer, amountOfTokens);
        require(sent, "Failed to sent the tokens");
        emit BuyTokens(buyer, amountOfEth, amountOfTokens);
    }

    // ToDo: create a withdraw() function that lets the owner withdraw ETH
    function withdraw() public onlyOwner {
        uint256 vendorBalance = address(this).balance;
        require(vendorBalance > 0, "Vendor balance is not enough");
        address owner = msg.sender;
        (bool sent, ) = owner.call{value: vendorBalance}("");
        require(sent, "");
    }

    // ToDo: create a sellTokens() function:
    function sellTokens(uint256 amount) public payable {
        require(amount > 0, "Selling amount should be greater then 0");
        address user = msg.sender;
        uint256 userBalance = yourToken.balanceOf(user);
        require(
            userBalance >= amount,
            "User do not have enough amount to sell"
        );
        uint256 amountOfEth = amount / tokensPerEth;
        uint256 vendorEthBalance = address(this).balance;
        require(
            vendorEthBalance >= amountOfEth,
            "Vendor do no have enough ETH"
        );
        bool sent = yourToken.transferFrom(user, address(this), amount);
        require(sent, "Failed to tranfer the amount");
        (bool ethSent, ) = user.call{value: amountOfEth}("");
        require(ethSent, "Failed to sent back eth");
        emit SellTokens(user, amountOfEth, amount);
    }
}
